// Class definition
var KTSelect2 = function() {
    // Private functions
    var forms_select2 = function() {
        $('#kt_select2_dekan, #kt_select2_dekan_validate').select2({
            placeholder: '- Pilih Dekan -'
        });

        $('#kt_select2_sk, #kt_select2_sk_validate').select2({
            placeholder: '- Pilih SK -'
        });
    }

    // Public functions
    return {
        init: function() {
            forms_select2();
        }
    };
}();

// Class definition
var KTBootstrapDatepicker = function () {

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    // Private functions
    var demos = function () {
        // minimum setup
        $('#kt_datepicker_terbit, #kt_datepicker_terbit_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_akhir, #kt_datepicker_mulai_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });

        $('#kt_datepicker_ttd, #kt_datepicker_akhir_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTSelect2.init();
    KTBootstrapDatepicker.init();
});