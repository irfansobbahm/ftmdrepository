'use strict';

// Class definition
var KTSelect2 = function() {
    // Private functions
    var forms_select2 = function() {
        $('#kt_select2_penerima, #kt_select2_penerima_validate').select2({
            placeholder: '- Pilih Nama -'
        });
    }

    // Public functions
    return {
        init: function() {
            forms_select2();
        }
    };
}();

var KTDatatablesDataSourceAjaxClient = function() {

	$.fn.dataTable.Api.register('column().title()', function() {
		return $(this.header()).text().trim();
	});

	var initTableNominatif = function() {
		var table = $('#kt_datatable_nominatif');

		// begin first table
		table.DataTable({
			responsive: true,
			ajax: {
				url: HOST_URL,
				type: 'POST',
				data: {
					pagination: {
						perpage: 50,
					},
				},
			},
			columns: [
				{data: 'no_sk'},
				{data: 'judul_sk'},
				{data: 'nama'},
				{data: 'no_identitas'},
				{data: 'jenis'},
				{data: 'total'},
				{data: 'keterangan_detail'},
				{data: 'tgl_pembayaran'},
				{data: 'status'},
			],
			columnDefs: [
				{
					targets: 5,
					render: function(data, type, full, meta) {
						return parseInt(data).toLocaleString(); ;
					}
				},
				{
					width: '75px',
					targets: -1,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Diajukan', 'state': 'warning'},
							2: {'title': 'Disetujui Ditkeu', 'state': 'info'},
							3: {'title': 'Ditransfer', 'state': 'success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
							'<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		});
	};

	return {
		//main function to initiate the module
		init: function() {
			initTableNominatif();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxClient.init();
	 KTSelect2.init();
});


