"use strict";
var KTDatatablesSearchOptionsAdvancedSearch = function() {

	$.fn.dataTable.Api.register('column().title()', function() {
		return $(this.header()).text().trim();
	});

	var initTable1 = function() {
		// begin first table
		var table = $('#kt_datatable').DataTable({
			responsive: true,
			// Pagination settings
			dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			// read more: https://datatables.net/examples/basic_init/dom.html

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Display _MENU_',
			},

			searchDelay: 500,
			processing: true,
			serverSide: true,
			ajax: {
				url: HOST_URL + '/api/sk_dekan',
				type: 'POST',
				data: {
					// parameters for custom backend script demo
					columnsDef: [
						'Nomor', 'Perihal', 'Penandatangan', 'TglTerbit', 'TglBerakhir',
						'NoSK', 'Kategori', 'Actions',],
				},
			},
			columns: [
				{data: 'Nomor'},
				{data: 'Perihal'},
				{data: 'Penandatangan'},
				{data: 'TglTerbit'},
				{data: 'TglBerakhir'},
				{data: 'NoSK'},				
				{data: 'Kategori'},
				{data: 'Actions', responsivePriority: -1},
			],

			initComplete: function() {
				this.api().columns().every(function() {
					var column = this;

					switch (column.title()) {
						case 'Tipe':
							var status = {
								1: {'title': 'Informasi', 'class': 'label-light-success'},
								2: {'title': 'Undangan', 'class': ' label-light-primary'},
								3: {'title': 'Permohonan', 'class': ' label-light-warning'},
								4: {'title': 'Lainnya', 'class': ' label-light-info'},
							};
							column.data().unique().sort().each(function(d, j) {
								$('.datatable-input[data-col-index="5"]').append('<option value="' + d + '">' + status[d].title + '</option>');
							});
							break;

						case 'Kategori':
							var status = {
								1: {'title': 'Sekretariat', 'state': 'primary'},
								2: {'title': 'Keuangan', 'state': 'primary'},
								3: {'title': 'Akademik', 'state': 'primary'},
								4: {'title': 'Kepegawaian', 'state': 'primary'},
								5: {'title': 'Sarpras', 'state': 'primary'},
								6: {'title': 'Lainnya', 'state': 'primary'},
							};
							column.data().unique().sort().each(function(d, j) {
								$('.datatable-input[data-col-index="6"]').append('<option value="' + d + '">' + status[d].title + '</option>');
							});
							break;
					}
				});
			},

			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return '\
							<div class="dropdown dropdown-inline">\
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">\
	                                <i class="la la-cog"></i>\
	                            </a>\
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
									<ul class="nav nav-hoverable flex-column">\
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>\
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-leaf"></i><span class="nav-text">Update Status</span></a></li>\
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-print"></i><span class="nav-text">Print</span></a></li>\
									</ul>\
							  	</div>\
							</div>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
								<i class="la la-edit"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
								<i class="la la-trash"></i>\
							</a>\
						';
					},
				},
				{
					targets: 5,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Informasi', 'class': 'label-light-success'},
								2: {'title': 'Undangan', 'class': ' label-light-primary'},
								3: {'title': 'Permohonan', 'class': ' label-light-warning'},
								4: {'title': 'Lainnya', 'class': ' label-light-info'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
					},
				},
				{
					targets: 6,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Sekretariat', 'state': 'primary'},
							2: {'title': 'Keuangan', 'state': 'primary'},
							3: {'title': 'Akademik', 'state': 'primary'},
							4: {'title': 'Kepegawaian', 'state': 'primary'},
							5: {'title': 'Sarpras', 'state': 'primary'},
							6: {'title': 'Lainnya', 'state': 'primary'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
							'<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		});

		var filter = function() {
			var val = $.fn.dataTable.util.escapeRegex($(this).val());
			table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
		};

		var asdasd = function(value, index) {
			var val = $.fn.dataTable.util.escapeRegex(value);
			table.column(index).search(val ? val : '', false, true);
		};

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			var params = {};
			$('.datatable-input').each(function() {
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});
			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				$(this).val('');
				table.column($(this).data('col-index')).search('', false, false);
			});
			table.table().draw();
		});

		$('#kt_datepicker').datepicker({
			todayHighlight: true,
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesSearchOptionsAdvancedSearch.init();
});
