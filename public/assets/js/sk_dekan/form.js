// Class definition
var SFormWidget = function() {
    // Private functions
    var selectdua = function() {
        // basic
        $('#asal_select2, #asal_select2_validate').select2({
            placeholder: '-Pilih Asal-'
        });
        $('#tujuan_select2, #tujuan_select2_validate').select2({
            placeholder: '-Pilih Tujuan-'
        });
        $('#penandatangan_select2, #penandatangan_select2_validate').select2({
            placeholder: '-Pilih Penandatangan-'
        });
        $('#kategori_select2, #kategori_select2_validate').select2({
            placeholder: '-Pilih Kategori-'
        });
        $('#institusi_select2, #institusi_select2_validate').select2({
            placeholder: '-Pilih Institusi-'
        });
        $('#tembusan_select2, #tembusan_select2_validate').select2({
            placeholder: '-Pilih Tembusan-'
        });


        $('[data-switch=true]').bootstrapSwitch();
    }

    var bswitch = function() {
        // minimum setup
        $('[data-switch=true]').bootstrapSwitch();
    };

    var dtpicker = function() {
        $('#tgl_masuk').datetimepicker({
            format: 'L',
            locale: 'id'
        });

        $('#tgl_acara').datetimepicker({
            format: 'L',
            locale: 'id'
        });

        // Demo 4
        $('#waktu_acara').datetimepicker({
            format: 'LT',
            locale: 'id'
        });
    };

    // Public functions
    return {
        init: function() {
            selectdua();
            dtpicker();
            bswitch();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    SFormWidget.init();

    $('#tipe').on('change', function() {
        if ($(this).val() == 2) {
            $('#acara-area').show();
        } else {
            $('#acara-area').hide();
        }
        
    })
});
