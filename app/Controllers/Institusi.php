<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Institusi extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
		$data = [
            'menu' => 'institusi',
            'submenu' => ''
        ];

		return view('institusi/index', $data);
	}

	public function tambah()
	{
		$data = [
            'menu' => 'institusi',
            'submenu' => 'tambah',
        ];

		return view('institusi/tambah', $data);
	}
}
