<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Sk_dekan extends BaseController
{
	use ResponseTrait;

	public function index()
	{
		$data = [
            'menu' => 'sk_dekan',
            'submenu' => ''
        ];

		return view('sk/sk_dekan', $data);
	}

	public function add()
	{
		$data = [
            'menu' => 'sk_dekan',
            'submenu' => 'add'
        ];

		return view('sk/tambah_sk_dekan', $data);
	}

	public function detail()
	{
		$data = [
            'menu' => 'sk_dekan',
            'submenu' => 'detail'
        ];

		return view('sk/detail_sk_dekan', $data);
	}
}
