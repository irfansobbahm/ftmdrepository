<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_dinas extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_dinas',
            'submenu' => 'masuk'
        ];

		return view('surat_dinas/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_dinas',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_dinas/tambah_dinas_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_dinas',
            'submenu' => 'keluar'
        ];

		return view('surat_dinas/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_dinas',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_dinas/tambah_dinas_keluar', $data);
	}
}
