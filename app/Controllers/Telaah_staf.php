<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Telaah_staf extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'telaah_staf',
            'submenu' => 'masuk'
        ];

		return view('telaah_staf/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'telaah_staf',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('telaah_staf/tambah_telaah_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'telaah_staf',
            'submenu' => 'keluar'
        ];

		return view('telaah_staf/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'telaah_staf',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('telaah_staf/tambah_telaah_keluar', $data);
	}
}
