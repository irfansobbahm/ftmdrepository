<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Perjanjian extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function internal()
	{
		$data = [
            'menu' => 'perjanjian',
            'submenu' => 'internal'
        ];

		return view('perjanjian/internal', $data);
	}

	public function tambah_internal()
	{
		$data = [
            'menu' => 'perjanjian',
            'submenu' => 'internal',
            'submenu_2' => 'tambah'
        ];

		return view('perjanjian/tambah_perjanjian_internal', $data);
	}

	public function eksternal()
	{
		$data = [
            'menu' => 'perjanjian',
            'submenu' => 'eksternal'
        ];

		return view('perjanjian/eksternal', $data);
	}

	public function tambah_eksternal()
	{
		$data = [
            'menu' => 'perjanjian',
            'submenu' => 'eksternal',
            'submenu_2' => 'tambah'
        ];

		return view('perjanjian/tambah_perjanjian_eksternal', $data);
	}
}
