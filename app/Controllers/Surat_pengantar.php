<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_pengantar extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_pengantar',
            'submenu' => 'masuk'
        ];

		return view('surat_pengantar/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_pengantar',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_pengantar/tambah_pengantar_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_pengantar',
            'submenu' => 'keluar'
        ];

		return view('surat_pengantar/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_pengantar',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_pengantar/tambah_pengantar_keluar', $data);
	}
}
