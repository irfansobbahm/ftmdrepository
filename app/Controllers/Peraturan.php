<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Peraturan extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function internal()
	{
		$data = [
            'menu' => 'peraturan',
            'submenu' => 'internal'
        ];

		return view('peraturan/internal', $data);
	}

	public function tambah_internal()
	{
		$data = [
            'menu' => 'peraturan',
            'submenu' => 'internal',
            'submenu_2' => 'tambah'
        ];

		return view('peraturan/tambah_peraturan_internal', $data);
	}

	public function eksternal()
	{
		$data = [
            'menu' => 'peraturan',
            'submenu' => 'eksternal'
        ];

		return view('peraturan/eksternal', $data);
	}

	public function tambah_eksternal()
	{
		$data = [
            'menu' => 'peraturan',
            'submenu' => 'eksternal',
            'submenu_2' => 'tambah'
        ];

		return view('peraturan/tambah_peraturan_eksternal', $data);
	}
}
