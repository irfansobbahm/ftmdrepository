<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_kuasa extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_kuasa',
            'submenu' => 'masuk'
        ];

		return view('surat_kuasa/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_kuasa',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_kuasa/tambah_kuasa_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_kuasa',
            'submenu' => 'keluar'
        ];

		return view('surat_kuasa/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_kuasa',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_kuasa/tambah_kuasa_keluar', $data);
	}
}
