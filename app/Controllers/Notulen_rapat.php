<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Notulen_rapat extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'notulen_rapat',
            'submenu' => 'masuk'
        ];

		return view('notulen_rapat/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'notulen_rapat',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('notulen_rapat/tambah_notulen_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'notulen_rapat',
            'submenu' => 'keluar'
        ];

		return view('notulen_rapat/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'notulen_rapat',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('notulen_rapat/tambah_notulen_keluar', $data);
	}
}
