<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Nodin_keluar extends BaseController
{
	use ResponseTrait;

	public function index()
	{
		$data = [
            'menu' => 'nodin',
            'submenu' => ''
        ];

		return view('nota_dinas/nota_dinas_masuk', $data);
	}

	public function add()
	{
		$data = [
            'menu' => 'nodin',
            'submenu' => 'add'
        ];

		return view('nota_dinas/tambah_nota_dinas_masuk', $data);
	}
}
