<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Api extends BaseController
{
	use ResponseTrait;

	public function surat_masuk()
	{
		//Tipe--> 1: informasi, 2: undangan, 3: permohonan, 4: lainnya
		//Kategori--> 1: sekretariat, 2: keuangan, 3: akademik, 4:kepegawaian, 5:sarpras, 6:lainnya  

		$data = [
           "recordsTotal"=> 7,
		    "recordsFiltered"=> 7,
		    "data"=> [
		        [
		            "Nomor"=> 1,
		            "Perihal"=> "Permohonan Perpanjangan Penerimaan SPP Belanja Honor Pegawai, Honor Jasa dan UYHD",
		            "Asal"=> "Fakultas Teknik Sipil dan Lingkungan",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "3946/IT1.C06.2/KU/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "2",
		            "Actions"=> null
		        ],
		        [
		           "Nomor"=> 2,
		            "Perihal"=> "Penyampaian Kelengkapan Dokumen untuk Pengajuan Pemberhentian Sementara dari Jabatan Fungsional",
		            "Asal"=> "Sekolah Bisnis dan Manajemen",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "5998/IT1.C09.2/KP/2021",
		            "Tipe"=> "1",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 3,
		            "Perihal"=> "Permohonan Perpanjangan Penyerapan Kegiatan NRNG - Ditdik NR",
		            "Asal"=> "Direktorat Pendidikan Non Reguler",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "588/IT1.B04.5/KU/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "2",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 4,
		            "Perihal"=> "Permohonan perpanjangan SPP",
		            "Asal"=> "Lembaga Penelitian dan Pengabdian kepada Masyarakat",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "5914/IT1.B07.1/KU.02/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "2",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 5,
		            "Perihal"=> "Undangan Progres Report dan Pembahasan Isue Implementasi Oracle, Jumat 12 November 2021",
		            "Asal"=> "Wakil Rektor Bidang Keuangan, Perencanaan, dan Pengembangan",
		            "TglMasuk"=> "10/11/2021",
		            "NoSurat"=> "1777/IT1.B06/TU.08/2021",
		            "Tipe"=> "2",
		            "Kategori"=> "1",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 6,
		            "Perihal"=> "Permohonan Perhitungan Beban Kerja",
		            "Asal"=> "Wakil Rektor Bidang Keuangan, Perencanaan, dan Pengembangan",
		            "TglMasuk"=> "10/11/2021",
		            "NoSurat"=> "1702/IT1.B06/LK/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 7,
		            "Perihal"=> "Permohonan data dosen berprestasi",
		            "Asal"=> "Badan Administrasi Umum dan Informasi",
		            "TglMasuk"=> "10/11/2021",
		            "NoSurat"=> "394/IT1.C06.2/KU/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		    ]
        ];

		return $this->setResponseFormat('json')->respond($data);
	}

	public function sk_dekan()
	{
		//Tipe--> 1: informasi, 2: undangan, 3: permohonan, 4: lainnya
		//Kategori--> 1: sekretariat, 2: keuangan, 3: akademik, 4:kepegawaian, 5:sarpras, 6:lainnya  

		$data = [
           "recordsTotal"=> 4,
		    "recordsFiltered"=> 4,
		    "data"=> [
		        [
		            "Nomor"=> 1,
		            "Perihal"=> "TIM PENYUSUN RENCANA STRATEGIS (RENSTRA) FAKULTAS TEKNIK MESIN DAN DIRGANTARA",
		            "Penandatangan"=> "Prof. Tatacipta Dirgantara",
		            "TglTerbit"=> "01/01/2021",
		            "TglBerakhir"=> "31/12/2021",
		            "NoSK"=> "230/IT1.C04/SK/KP/2021",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		        [
		           "Nomor"=> 2,
		            "Perihal"=> "KOMISI PROGRAM PASCASARJANA (KPPS) FAKULTAS TEKNIK MESIN DAN DIRGANTARA",
		            "Penandatangan"=> "Prof. Tatacipta Dirgantara",
		            "TglTerbit"=> "01/01/2021",
		            "TglBerakhir"=> "31/12/2021",
		            "NoSK"=> "01B/IT1.C04/SK-KP/2021",
		            "Kategori"=> "3",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 3,
		            "Perihal"=> "KEPALA LABORATORIUM FAKULTAS TEKNIK MESIN DAN DIRGANTARA",
		            "Penandatangan"=> "Prof. Tatacipta Dirgantara",
		            "TglTerbit"=> "01/01/2021",
		            "TglBerakhir"=> "31/12/2021",
		            "NoSK"=> "231/IT1.C04/SK/KP/2021",
		            "Kategori"=> "3",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 4,
		            "Perihal"=> "TIM PELAKSANA KEGIATAN PENELITIAN, PENGABDIAN MASYARAKAT DAN INOVASI (PPMI) FAKULTAS TEKNIK MESIN DAN DIRGANTARA",
		            "Penandatangan"=> "Prof. Tatacipta Dirgantara",
		            "TglTerbit"=> "01/01/2021",
		            "TglBerakhir"=> "31/12/2021",
		            "NoSK"=> "2C/IT1.C04/SK/KP/2021",
		            "Kategori"=> "3",
		            "Actions"=> null
		        ],
		    ]
        ];

		return $this->setResponseFormat('json')->respond($data);
	}

	public function detail_sk_dekan()
	{
		//Tipe--> 1: informasi, 2: undangan, 3: permohonan, 4: lainnya
		//Kategori--> 1: sekretariat, 2: keuangan, 3: akademik, 4:kepegawaian, 5:sarpras, 6:lainnya  

		$data = [
           "recordsTotal"=> 4,
		    "recordsFiltered"=> 4,
		    "data"=> [
		        [
		            "Nomor"=> 1,
		            "NIP"=> "197004242006041018",
		            "Nama"=> "Prof. Tatacipta Dirgantara",
		            "Jabatan"=> "Ketua",
		        ],
		        [
		           "Nomor"=> 2,
		            "NIP"=> "197004242006041018",
		            "Nama"=> "Dr. Hermawan Judhawisastra",
		            "Jabatan"=> "Sekretaris",
		        ],
		        [
		            "Nomor"=> 3,
		            "NIP"=> "197004242006041018",
		            "Nama"=> "Dr. Lavi R. Zuhal",
		            "Jabatan"=> "Anggota",
		        ],
		        [
		            "Nomor"=> 4,
		            "NIP"=> "197004242006041018",
		            "Nama"=> "Ferryanto, MT.",
		            "Jabatan"=> "Ketua",
		        ],
		    ]
        ];

		return $this->setResponseFormat('json')->respond($data);
	}
}
