<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Berita_acara extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'berita_acara',
            'submenu' => 'masuk'
        ];

		return view('berita_acara/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'berita_acara',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('berita_acara/tambah_berita_acara_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'berita_acara',
            'submenu' => 'keluar'
        ];

		return view('berita_acara/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'berita_acara',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('berita_acara/tambah_berita_acara_keluar', $data);
	}
}
