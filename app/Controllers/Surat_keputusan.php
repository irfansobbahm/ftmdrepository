<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_keputusan extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }
    
	public function index()
	{
		
	}

	public function internal()
	{
		$data = [
            'menu' => 'keputusan',
            'submenu' => 'internal'
        ];

		return view('keputusan/internal', $data);
	}

	public function tambah_internal()
	{
		$data = [
            'menu' => 'keputusan',
            'submenu' => 'internal',
            'submenu_2' => 'tambah'
        ];

		return view('keputusan/tambah_keputusan_internal', $data);
	}

	public function eksternal()
	{
		$data = [
            'menu' => 'keputusan',
            'submenu' => 'eksternal'
        ];

		return view('keputusan/eksternal', $data);
	}

	public function tambah_eksternal()
	{
		$data = [
            'menu' => 'keputusan',
            'submenu' => 'eksternal',
            'submenu_2' => 'tambah'
        ];

		return view('keputusan/tambah_keputusan_eksternal', $data);
	}
}
