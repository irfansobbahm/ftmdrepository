<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Instruksi extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function internal()
	{
		$data = [
            'menu' => 'instruksi',
            'submenu' => 'internal'
        ];

		return view('instruksi/internal', $data);
	}

	public function tambah_internal()
	{
		$data = [
            'menu' => 'instruksi',
            'submenu' => 'internal',
            'submenu_2' => 'tambah'
        ];

		return view('instruksi/tambah_instruksi_internal', $data);
	}

	public function eksternal()
	{
		$data = [
            'menu' => 'instruksi',
            'submenu' => 'eksternal'
        ];

		return view('instruksi/eksternal', $data);
	}

	public function tambah_eksternal()
	{
		$data = [
            'menu' => 'instruksi',
            'submenu' => 'eksternal',
            'submenu_2' => 'tambah'
        ];

		return view('instruksi/tambah_instruksi_eksternal', $data);
	}
}
