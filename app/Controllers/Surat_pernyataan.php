<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_pernyataan extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_pernyataan',
            'submenu' => 'masuk'
        ];

		return view('surat_pernyataan/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_pernyataan',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_pernyataan/tambah_pernyataan_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_pernyataan',
            'submenu' => 'keluar'
        ];

		return view('surat_pernyataan/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_pernyataan',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_pernyataan/tambah_pernyataan_keluar', $data);
	}
}
