<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
	public function index()
	{
		$data = [
            'menu' => 'dashboard',
            'submenu' => ''
        ];

		return view('dashboard', $data);
	}
}
