<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Laporan extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'laporan',
            'submenu' => 'masuk'
        ];

		return view('laporan/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'laporan',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('laporan/tambah_laporan_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'laporan',
            'submenu' => 'keluar'
        ];

		return view('laporan/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'laporan',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('laporan/tambah_laporan_keluar', $data);
	}
}
