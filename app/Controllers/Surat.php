<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat extends BaseController
{
	use ResponseTrait;

	public function index()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => ''
        ];

		return view('surat/index', $data);
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => 'masuk'
        ];

		return view('surat/masuk', $data);
	}

	public function add_masuk()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => 'masuk'
        ];

		return view('surat/add_masuk', $data);
	}

	public function masuk_detail()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => 'masuk'
        ];

		return view('surat/detail_masuk', $data);
	}

	public function masuk_detail_disposisi()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => 'masuk'
        ];

		return view('surat/detail_disposisi_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => 'keluar'
        ];

		return view('surat/keluar', $data);
	}

	public function add_keluar()
	{
		$data = [
            'menu' => 'surat',
            'submenu' => 'keluar'
        ];

		return view('surat/add_keluar', $data);
	}

	public function api_surat_masuk()
	{
		//Tipe--> 1: informasi, 2: undangan, 3: permohonan, 4: lainnya
		//Kategori--> 1: sekretariat, 2: keuangan, 3: akademik, 4:kepegawaian, 5:sarpras, 6:lainnya  

		$data = [
           "recordsTotal"=> 7,
		    "recordsFiltered"=> 7,
		    "data"=> [
		        [
		            "Nomor"=> 1,
		            "Perihal"=> "Permohonan Perpanjangan Penerimaan SPP Belanja Honor Pegawai, Honor Jasa dan UYHD",
		            "Asal"=> "Fakultas Teknik Sipil dan Lingkungan",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "3946/IT1.C06.2/KU/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "2",
		            "Actions"=> null
		        ],
		        [
		           "Nomor"=> 2,
		            "Perihal"=> "Penyampaian Kelengkapan Dokumen untuk Pengajuan Pemberhentian Sementara dari Jabatan Fungsional",
		            "Asal"=> "Sekolah Bisnis dan Manajemen",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "5998/IT1.C09.2/KP/2021",
		            "Tipe"=> "1",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 3,
		            "Perihal"=> "Permohonan Perpanjangan Penyerapan Kegiatan NRNG - Ditdik NR",
		            "Asal"=> "Direktorat Pendidikan Non Reguler",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "588/IT1.B04.5/KU/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "2",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 4,
		            "Perihal"=> "Permohonan perpanjangan SPP",
		            "Asal"=> "Lembaga Penelitian dan Pengabdian kepada Masyarakat",
		            "TglMasuk"=> "15/11/2021",
		            "NoSurat"=> "5914/IT1.B07.1/KU.02/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "2",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 5,
		            "Perihal"=> "Undangan Progres Report dan Pembahasan Isue Implementasi Oracle, Jumat 12 November 2021",
		            "Asal"=> "Wakil Rektor Bidang Keuangan, Perencanaan, dan Pengembangan",
		            "TglMasuk"=> "10/11/2021",
		            "NoSurat"=> "1777/IT1.B06/TU.08/2021",
		            "Tipe"=> "2",
		            "Kategori"=> "1",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 6,
		            "Perihal"=> "Permohonan Perhitungan Beban Kerja",
		            "Asal"=> "Wakil Rektor Bidang Keuangan, Perencanaan, dan Pengembangan",
		            "TglMasuk"=> "10/11/2021",
		            "NoSurat"=> "1702/IT1.B06/LK/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		        [
		            "Nomor"=> 7,
		            "Perihal"=> "Permohonan data dosen berprestasi",
		            "Asal"=> "Badan Administrasi Umum dan Informasi",
		            "TglMasuk"=> "10/11/2021",
		            "NoSurat"=> "394/IT1.C06.2/KU/2021",
		            "Tipe"=> "3",
		            "Kategori"=> "4",
		            "Actions"=> null
		        ],
		    ]
        ];

		return $this->setResponseFormat('json')->respond($data);
	}
}
