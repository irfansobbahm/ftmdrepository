<?php

namespace App\Controllers;
use App\Models\PegawaiModel;
use App\Libraries\ITBCas;

class Auth extends BaseController
{
	public function index()
	{
		// $cas = new ITBCas();
        // $cas->loginITB();

        // $sess = session()->get('phpCAS');
        // $attr = $sess['attributes'];

        // if (is_array($attr['itbNIP'])) {
		// 	$n = $attr['itbNIP'];
		// 	if (strlen($n[0]) > strlen($n[1])) {
		// 		$nip = $n[0];
		// 	} else {
		// 		$nip = $n[1];
		// 	}
    	// } else {
    	// 	$nip = $attr['itbNIP'];
    	// }

    	// $user_role = $model->select('t_pegawai.nip, t_pegawai.nama, t_repository_role.role')
		// 								->join('t_repository_role', 't_repository_role.nip = t_pegawai.nip', 'left')
		// 								->where(['t_pegawai.nip' => $nip])
		// 								->first();

        // if (is_array($user_role)) {		

	    // 	$this->setUserMethod([
		// 			'uid' => $sess['user'],
		// 			'nip' => $nip,
		// 			'cn' => $attr['cn'],
		// 			'role' => is_array($user_role) ? $user_role['role'] : 'user'
		// 		]);

		// 	return redirect()->to(base_url('dashboard'));
		// }


		

		$data = [];
		helper(['form']);

		if ($this->request->getMethod() == 'post')
		{
			//validation here
			$rules = [
				'username' => 'required|min_length[3]',
				'password' => 'required|min_length[1]|validateUser[username,password]',
			];

			$errors = [
				'password' => [
					'validateUser' => 'Username dan Password tidak sesuai'
				]
			];

			if (!$this->validate($rules, $errors))
			{
				$data['validation'] = $this->validator;
			}
			else
			{
				//mendapatkan data user
				// print_r("masuk");die;
				$model 	= new PegawaiModel();
				$nama	= $this->request->getVar('username');
				$getnip = $model->getPegawaiByNip($nama);

				$user 	= $model->select('t_pegawai.nip, t_pegawai.nama, t_repository_role.role')
										->join('t_repository_role', 't_repository_role.nip = t_pegawai.nip', 'left')
										->where(['t_pegawai.nip' => $getnip['nip']])
										->first();
				// Eksekusi query dan simpan hasilnya dalam variabel $user
// 				$user = $model->select('t_pegawai.nip, t_pegawai.nama, t_repository_role.role')
// 				->join('t_repository_role', 't_repository_role.nip = t_pegawai.nip', 'left')
// 				->where(['t_pegawai.nip' => $getnip['nip']])
// 				->first();

// // 				// Tampilkan hasil query
// 				$query = $model->getLastQuery();
// 				echo$query;

// // 				// Tampilkan hasil data user
// 				// print_r($user);
// die;

				// set model session
				$this->setUserMethod($user);
				return redirect()->to(base_url('dashboard'));
			}
		}

		return view('layout/login', $data);
	}

	public function signout()
	{
		session()->destroy();
		return redirect()->to(base_url());

		// $cas = new ITBCas();
        // $cas->logoutSSO();
	}

	private function setUserMethod($user)
	{
		$data = [
			'nip' => $user['nip'],
			'nama' => $user['nama'],
			'role' => ($user['role'] != '') ? $user['role'] : 'DOSEN',
			'isLoggedIn' => true
		];

		session()->set($data);
		return true;
	}
}
