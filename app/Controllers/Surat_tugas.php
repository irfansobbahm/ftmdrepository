<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_tugas extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_tugas',
            'submenu' => 'masuk'
        ];

		return view('surat_tugas/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_tugas',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_tugas/tambah_tugas_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_tugas',
            'submenu' => 'keluar'
        ];

		return view('surat_tugas/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_tugas',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_tugas/tambah_tugas_keluar', $data);
	}
}
