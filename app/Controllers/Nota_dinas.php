<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Nota_dinas extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'nota_dinas',
            'submenu' => 'masuk'
        ];

		return view('nota_dinas/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'nota_dinas',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('nota_dinas/tambah_dinas_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'nota_dinas',
            'submenu' => 'keluar'
        ];

		return view('nota_dinas/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'nota_dinas',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('nota_dinas/tambah_dinas_keluar', $data);
	}
}
