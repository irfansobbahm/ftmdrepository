<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Nota_kesepahaman extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function internal()
	{
		$data = [
            'menu' => 'nota_kesepahaman',
            'submenu' => 'internal'
        ];

		return view('nota_kesepahaman/internal', $data);
	}

	public function tambah_internal()
	{
		$data = [
            'menu' => 'nota_kesepahaman',
            'submenu' => 'internal',
            'submenu_2' => 'tambah'
        ];

		return view('nota_kesepahaman/tambah_nota_kesepahaman_internal', $data);
	}

	public function eksternal()
	{
		$data = [
            'menu' => 'nota_kesepahaman',
            'submenu' => 'eksternal'
        ];

		return view('nota_kesepahaman/eksternal', $data);
	}

	public function tambah_eksternal()
	{
		$data = [
            'menu' => 'nota_kesepahaman',
            'submenu' => 'eksternal',
            'submenu_2' => 'tambah'
        ];

		return view('nota_kesepahaman/tambah_nota_kesepahaman_eksternal', $data);
	}
}
