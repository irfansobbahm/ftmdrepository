<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_keterangan extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_keterangan',
            'submenu' => 'masuk'
        ];

		return view('surat_keterangan/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_keterangan',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_keterangan/tambah_keterangan_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_keterangan',
            'submenu' => 'keluar'
        ];

		return view('surat_keterangan/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_keterangan',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_keterangan/tambah_keterangan_keluar', $data);
	}
}
