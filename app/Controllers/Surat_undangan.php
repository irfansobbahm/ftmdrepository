<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_undangan extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_undangan',
            'submenu' => 'masuk'
        ];

		return view('surat_undangan/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_undangan',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_undangan/tambah_undangan_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_undangan',
            'submenu' => 'keluar'
        ];

		return view('surat_undangan/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_undangan',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_undangan/tambah_undangan_keluar', $data);
	}
}
