<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Dokumen_pendukung extends BaseController
{
	use ResponseTrait;

	public function index()
	{
		$data = [
            'menu' => 'dokumen',
            'submenu' => ''
        ];

		return view('dokumen/index', $data);
	}

	public function add()
	{
		$data = [
            'menu' => 'dokumen',
            'submenu' => 'add'
        ];

		return view('dokumen/tambah', $data);
	}
}
