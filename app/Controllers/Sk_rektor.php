<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Sk_rektor extends BaseController
{
	use ResponseTrait;

	public function index()
	{
		$data = [
            'menu' => 'sk_rektor',
            'submenu' => ''
        ];

		return view('sk/sk_rektor', $data);
	}

	public function add()
	{
		$data = [
            'menu' => 'sk_rektor',
            'submenu' => 'add'
        ];

		return view('sk/tambah_sk_rektor', $data);
	}
}
