<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Surat_edaran extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function masuk()
	{
		$data = [
            'menu' => 'surat_edaran',
            'submenu' => 'masuk'
        ];

		return view('surat_edaran/masuk', $data);
	}

	public function tambah_masuk()
	{
		$data = [
            'menu' => 'surat_edaran',
            'submenu' => 'masuk',
            'submenu_2' => 'tambah'
        ];

		return view('surat_edaran/tambah_edaran_masuk', $data);
	}

	public function keluar()
	{
		$data = [
            'menu' => 'surat_edaran',
            'submenu' => 'keluar'
        ];

		return view('surat_edaran/keluar', $data);
	}

	public function tambah_keluar()
	{
		$data = [
            'menu' => 'surat_edaran',
            'submenu' => 'keluar',
            'submenu_2' => 'tambah'
        ];

		return view('surat_edaran/tambah_edaran_keluar', $data);
	}
}
