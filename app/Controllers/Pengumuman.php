<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Pengumuman extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function internal()
	{
		$data = [
            'menu' => 'pengumuman',
            'submenu' => 'internal'
        ];

		return view('pengumuman/internal', $data);
	}

	public function tambah_internal()
	{
		$data = [
            'menu' => 'pengumuman',
            'submenu' => 'internal',
            'submenu_2' => 'tambah'
        ];

		return view('pengumuman/tambah_pengumuman_internal', $data);
	}

	public function eksternal()
	{
		$data = [
            'menu' => 'pengumuman',
            'submenu' => 'eksternal'
        ];

		return view('pengumuman/eksternal', $data);
	}

	public function tambah_eksternal()
	{
		$data = [
            'menu' => 'pengumuman',
            'submenu' => 'eksternal',
            'submenu_2' => 'tambah'
        ];

		return view('pengumuman/tambah_pengumuman_eksternal', $data);
	}
}
