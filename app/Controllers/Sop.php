<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class Sop extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
        
    }

	public function index()
	{
		
	}

	public function itb()
	{
		$data = [
            'menu' => 'sop',
            'submenu' => 'itb'
        ];

		return view('sop/itb', $data);
	}

	public function tambah_itb()
	{
		$data = [
            'menu' => 'sop',
            'submenu' => 'itb',
            'submenu_2' => 'tambah'
        ];

		return view('sop/tambah_sop_itb', $data);
	}

	public function ftmd()
	{
		$data = [
            'menu' => 'sop',
            'submenu' => 'ftmd'
        ];

		return view('sop/ftmd', $data);
	}

	public function tambah_ftmd()
	{
		$data = [
            'menu' => 'sop',
            'submenu' => 'ftmd',
            'submenu_2' => 'tambah'
        ];

		return view('sop/tambah_sop_ftmd', $data);
	}
}
