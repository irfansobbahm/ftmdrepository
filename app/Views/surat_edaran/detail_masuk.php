<?php $this->extend('layout/index') ?>

<?php $this->section('stylesheet') ?>
<?php $this->endSection() ?>

<?php $this->section('script') ?>
    <script>var HOST_URL = "<?=base_url()?>";</script>
    <script src="/assets/js/surat_masuk/form.js"></script>
<?php $this->endSection() ?>

<?php $this->section('subheader-info') ?>
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Detail Surat Masuk</h5>
    <!--end::Page Title-->
    <!--begin::Breadcrumb-->
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item text-muted">
            <a href="" class="text-muted">Surat Masuk</a>
        </li>
        <li class="breadcrumb-item text-muted">
            <a href="" class="text-muted">Detail Surat Masuk</a>
        </li>
    </ul>
    <!--end::Breadcrumb-->
<?php $this->endSection() ?>

<?php $this->section('subheader-toolbar') ?>
    <a href="/surat/masuk" class="btn btn-sm btn-primary">
        <i class="flaticon2-left-arrow-1"></i> Kembali
    </a>&nbsp;&nbsp;
    <a href="#" class="btn btn-sm btn-success">
        <i class="flaticon-download"></i> Donwload File Surat
    </a>&nbsp;&nbsp;
    <a href="/surat/masuk_detail_disposisi" class="btn btn-sm btn-warning">
        <i class="flaticon-share"></i> Detail Disposisi Surat
    </a>
<?php $this->endSection() ?>



<?php $this->section('content') ?>
    <div class="container">
        <!--begin::Notice-->
        <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert" style="display:none;">
            <div class="alert-icon">
                <span class="svg-icon svg-icon-primary svg-icon-xl">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3" />
                            <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero" />
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
            </div>
            <div class="alert-text">This example is almost identical to text based individual column example and provides the same functionality. With server-side processing enabled, all paging, searching, ordering actions that DataTables performs are handed off to a server where an SQL engine (or similar) can perform these actions on the large data set. See official documentation
            <a class="font-weight-bold" href="https://datatables.net/examples/api/multi_filter_select.html" target="_blank">here</a>.</div>
        </div>
        <!--end::Notice-->
        <!--begin::Card-->
        <div class="card card-custom gutter-b example example-compact">
            <!-- <div class="card-header">
                <h3 class="card-title">Base Controls</h3>
                <div class="card-toolbar">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div> -->
            <!--begin::Form-->
            <form>
                <div class="card-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <label>No. Surat</label>
                                <input type="text" class="form-control" placeholder="No. Surat" disabled="disabled" value="3946/IT1.C06.2/KU/2021" />
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label>Tanggal Masuk</label>
                                <div class="input-group input-group-solid date" id="tgl_masuk" data-target-input="nearest">
                                    <input type="text" class="form-control form-control-solid datetimepicker-input" placeholder="Pilih tanggal" data-target="#tgl_masuk" disabled="disabled" value="15/11/2021" />
                                    <div class="input-group-append" data-target="#tgl_masuk" data-toggle="datetimepicker">
                                        <span class="input-group-text">
                                            <i class="ki ki-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <label>Asal</label>
                                <input type="text" class="form-control" disabled="disabled" value="Fakultas Teknik Sipil dan Lingkungan" />
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label>Tujuan
                                <span class="text-danger">*</span></label>
                                <select class="form-control" id="tujuan_select2" name="asal" disabled="disabled">
                                    <option value=""></option>
                                    <option value="Dekan">Dekan</option>
                                    <option value="WDS" selected="selected">Wakil Dekan Bidang Sumberdaya</option>
                                    <option value="WDA">Wakil Dekan Bidang Akademik</option>
                                    <option value="Kabag">Kepala Bagian</option>
                                </select>
                            </div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label>Tembusan</label>
                        <select class="form-control" id="tembusan_select2" name="tembusan" multiple="multiple" disabled="disabled">
                            <option value=""></option>
                            <option value="Dekan" selected="selected">Dekan</option>
                            <option value="WDS">Wakil Dekan Bidang Sumberdaya</option>
                            <option value="WDA" selected="selected">Wakil Dekan Bidang Akademik</option>
                            <option value="Kabag">Kepala Bagian</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Perihal</label>
                        <input type="text" class="form-control" placeholder="Perihal" disabled="disabled" value="Permohonan Perpanjangan Penerimaan SPP Belanja Honor Pegawai, Honor Jasa dan UYHD  " />
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea class="form-control" id="deskripsi" rows="3" disabled="disabled"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <label>Tipe
                                <span class="text-danger">*</span></label>
                                <select class="form-control" name="tipe" id="tipe" disabled="disabled">
                                    <option value="1">Informasi</option>
                                    <option value="2">Undangan</option>
                                    <option value="3" selected="selected">Permohonan</option>
                                    <option value="4">Lainnya</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label>Kategori
                                <span class="text-danger">*</span></label>
                                <select class="form-control" id="kategori_select2" name="kategori" multiple="multiple" disabled="disabled">
                                    <option value=""></option>
                                    <option value="1">Sekretariat</option>
                                    <option value="2" selected="selected">Keuangan</option>
                                    <option value="3">Akademik</option>
                                    <option value="4">Kepegawaian</option>
                                    <option value="5">Sarana Prasarana</option>
                                    <option value="6">Lainnya</option>
                                </select>
                            </div>
                        </div>                        
                    </div>
                    <!-- <div class="form-group" style="display:none;" id="acara-area">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <label>Tanggal Kegiatan/Acara</label>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="input-group input-group-solid date" id="tgl_acara" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-solid datetimepicker-input" placeholder="Pilih tanggal" data-target="#tgl_acara" />
                                            <div class="input-group-append" data-target="#tgl_acara" data-toggle="datetimepicker">
                                                <span class="input-group-text">
                                                    <i class="ki ki-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="input-group input-group-solid date" id="waktu_acara" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-solid datetimepicker-input" placeholder="Pilih waktu" data-target="#waktu_acara" />
                                            <div class="input-group-append" data-target="#waktu_acara" data-toggle="datetimepicker">
                                                <span class="input-group-text">
                                                    <i class="ki ki-clock"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label>Tempat</label>
                                <input type="text" class="form-control" placeholder="Tempat Kegiatan/Acara" />
                            </div>
                        </div>
                        
                    </div> -->
                    <div class="form-group">
                        <label>Sifat Surat</label>
                        <div><input data-switch="true" type="checkbox" data-on-text="Segera" data-on-color="danger" data-handle-width="50" data-off-text="Biasa" data-off-color="primary" class="form-control" disabled="disabled" value="false" /></div>
                    </div>
                </div>
                <!-- <div class="card-footer">
                    <a href="/surat/disposisi_masuk"  class="btn btn-primary mr-2">Disposisikan Surat</a>
                </div> -->
            </form>
        <!--end::Card-->
    </div>
<?php $this->endSection() ?>