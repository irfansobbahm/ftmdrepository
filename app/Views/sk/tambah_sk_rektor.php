<?php $this->extend('layout/index') ?>

<?php $this->section('stylesheet') ?>
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
    <script>var HOST_URL = "<?=base_url()?>";</script>
<?php $this->endSection() ?>

<?php $this->section('subheader-info') ?>
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Tambah Surat Keputusan Rektor</h5>
    <!--end::Page Title-->
    <!--begin::Breadcrumb-->
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item text-muted">
            <a href="" class="text-muted">Surat Keputusan Rektor</a>
        </li>
        <li class="breadcrumb-item text-muted">
            <a href="" class="text-muted">Tambah Surat Keputusan Rektor</a>
        </li>
    </ul>
    <!--end::Breadcrumb-->
<?php $this->endSection() ?>

<?php $this->section('subheader-toolbar') ?>
    <a href="/sk_rektor" class="btn btn-sm btn-primary">
        <i class="flaticon2-left-arrow-1"></i> Kembali
    </a>
<?php $this->endSection() ?>


<?php $this->section('content') ?>
    <div class="container">
    </div>
<?php $this->endSection() ?>   