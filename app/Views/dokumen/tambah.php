<?php $this->extend('layout/index') ?>

<?php $this->section('stylesheet') ?>
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?php $this->endSection() ?>

<?php $this->section('script') ?>
    <script>var HOST_URL = "<?=base_url()?>";</script>
    <script src="/assets/js/dokumen/form.js"></script>
<?php $this->endSection() ?>

<?php $this->section('subheader-info') ?>
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Tambah Dokumen Pendukung</h5>
    <!--end::Page Title-->
    <!--begin::Breadcrumb-->
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item text-muted">
            <a href="" class="text-muted">Dokumen Pendukung</a>
        </li>
        <li class="breadcrumb-item text-muted">
            <a href="" class="text-muted">Tambah Dokumen Pendukung</a>
        </li>
    </ul>
    <!--end::Breadcrumb-->
<?php $this->endSection() ?>

<?php $this->section('subheader-toolbar') ?>
    <a href="/dokumen_pendukung" class="btn btn-sm btn-primary">
        <i class="flaticon2-left-arrow-1"></i> Kembali
    </a>
<?php $this->endSection() ?>


<?php $this->section('content') ?>
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom gutter-b example example-compact">
            <!-- <div class="card-header">
                <h3 class="card-title">Base Controls</h3>
                <div class="card-toolbar">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div> -->
            <!--begin::Form-->
            <form>
                <div class="card-body">
                    <div class="form-group">
                        <label>Perihal
                        <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Perihal" />
                    </div>
                    <div class="form-group">
                        <label>Deskripsi
                        <span class="text-danger">*</span></label>
                        <textarea class="form-control" id="deskripsi" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Hak Akses (Pejabat Struktural)
                        <span class="text-danger">*</span></label>
                        <select class="form-control" id="tujuan_select2" name="asal">
                            <option value=""></option>
                            <option value="Dekan">Dekan</option>
                            <option value="WDS">Wakil Dekan Bidang Sumberdaya</option>
                            <option value="WDA">Wakil Dekan Bidang Akademik</option>
                            <option value="Kabag">Kepala Bagian</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Hak Akses (Personal)</label>
                        <select class="form-control" id="tembusan_select2" name="tembusan" multiple="multiple">
                            <option value=""></option>
                            <option value="Dekan">Prof. Tatacipta Dirgantara</option>
                            <option value="WDS">Dr. Hermawan Yudhawisastra</option>
                            <option value="WDA">Dr. Lavi R. Zuhal</option>
                            <option value="Kabag">Ferryanto, M.T.</option>
                            <option value="Kabag">Miftah Kamaluddin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kategori
                        <span class="text-danger">*</span></label>
                        <select class="form-control" id="kategori_select2" name="kategori" multiple="multiple">
                            <option value=""></option>
                            <option value="1">Sekretariat</option>
                            <option value="2">Keuangan</option>
                            <option value="3">Akademik</option>
                            <option value="4">Kepegawaian</option>
                            <option value="5">Sarana Prasarana</option>
                            <option value="6">Lainnya</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>File</label>
                        <div></div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile" />
                            <label class="custom-file-label" for="customFile">Pilih file</label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-primary mr-2">Simpan</button>
                    <button type="reset" class="btn btn-secondary">Batal</button>
                </div>
            </form>
        <!--end::Card-->
    </div>
<?php $this->endSection() ?>   